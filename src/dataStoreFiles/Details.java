package dataStoreFiles;
import java.util.Scanner;
public class Details {
	private String key;
	private long mobileNumber;
	private long creation_time;
	private long time_to_live = 0;
	
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public long getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public long getCreation_time() {
		return creation_time;
	}
	public void setCreation_time(long creation_time) {
		this.creation_time = creation_time;
	}
	public long getTime_to_live() {
		return time_to_live;
	}
	public void setTime_to_live(long time_to_live) {
		this.time_to_live = time_to_live;
	}
}
